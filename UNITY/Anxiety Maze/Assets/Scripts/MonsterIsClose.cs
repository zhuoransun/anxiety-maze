﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterIsClose : MonoBehaviour {
    //public AudioClip scaredBoy;
    AudioSource audioSource;
   // public Transform Monster;
	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();	
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            audioSource.Play();
        }
        else {
            audioSource.Stop();
        }
    }
   // private void OnTriggerExit(Collider other)
   // {
       // if (other.tag == "Player")
          //  audioSource.Stop();
   // }
    // Update is called once per frame
    void Update () {
		
	}
}
