﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleMover : MonoBehaviour {
    public float maxSize;
    public float scaler;
    public float waitTime;

    void Start()
    {
        StartCoroutine(Scale());
    }

    IEnumerator Scale() {

        float time = 0;

        while (true) {
            while (maxSize > transform.localScale.z) {
                time += Time.deltaTime;
                transform.localScale += new Vector3(0,0, 1) * Time.deltaTime * scaler;
                yield return null;
            }


        }



    }

    void Update()
    {
   
    }



}
