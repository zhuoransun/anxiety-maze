﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManager : MonoBehaviour {
    public int scene;

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // SceneManager.LoadScene(scene);
            Application.LoadLevel(scene);
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}
