﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneChangeManager : MonoBehaviour
{
    public int scene;

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
        }
    }
}
