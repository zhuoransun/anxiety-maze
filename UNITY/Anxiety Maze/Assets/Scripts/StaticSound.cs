﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticSound : MonoBehaviour {

    public Transform Monster;

    AudioSource audioSource;
    public float intensity;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(AdjustVolume());


    }
    IEnumerator AdjustVolume()
    {
        while (true)
        {

            if (audioSource.isPlaying)
            { //do this only if some audio is being played in this gameObject's AudioSource
                float distanceToTarget = Vector3.Distance(transform.position, Monster.position); // Assuming that the target is the player or the audio listener
                if (distanceToTarget < 1) { distanceToTarget = 1; }
                audioSource.volume = 1 / distanceToTarget; // this works as a linear function
                yield return new WaitForSeconds(0.1f); // this will adjust the volume based on distance every 0.1 seconds (instead of every frame)
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
