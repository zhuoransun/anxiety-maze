﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class DistanceStatic : MonoBehaviour {
    //If monster is at this transform, Static will increase, sound static will increase too
    public Transform Monster;
    NoiseAndGrain nag;
    public float intensity;
   
    
    // Use this for initialization
	void Start () {
       float dist = Vector3.Distance(Monster.position, transform.position);
        nag = GetComponent<NoiseAndGrain>();
        
    }
	
	// Update is called once per frame
	void Update () {
        float dist = Vector3.Distance(Monster.position, transform.position);
        if (dist < 1) { dist = 1; }
        nag.intensityMultiplier = intensity / dist;
      

    }
           

        }
    

